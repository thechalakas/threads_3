﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Threads_3
{
    class Program
    {
        public static int sleep_duration = 1000;

        //for some reason, this only takes object type paramters, if I want to use 
        //it with the parameterized thread start
        static void thread_demo(object loop_index)
        {
            int loop_index_cast = (int)loop_index;
            for (int i = 0; i < loop_index_cast; i++)
            {
                Console.WriteLine("thread_demo - value of i is {0}", i);
                //making the thread to go sleep
                Thread.Sleep(sleep_duration);
            }
        }

        static void Main(string[] args)
        {
            //creating a new thread with the above method
            Thread t = new Thread(new ParameterizedThreadStart(thread_demo));
            //I will also send a paramater when starting the thread
            t.Start(10);

            //stop before continuing to the next part where I am looking at thread stopping
            Console.ReadLine();

            //this will be the shared variable
            bool shoud_i_stop = false;

            //a new thread with a lambda expression defined method
            Thread t2 = new Thread(new ThreadStart( () =>
                    {
                        //as long as the shared variable is false
                        //this loop remains active
                        while (shoud_i_stop == false)
                        {
                            Console.WriteLine("The loop is still running");
                            Thread.Sleep(1000);
                        }

                    }
                ));
            //start the thread
            t2.Start();

            //at this point, the thread has started
            //in the thread, we have a infinite loop
            
            //at the same time, you must know that the while loop is running in a separate thread       
            
            //the following line is the main thread that is waiting for some key to be pressed             
            //this wait continues on the main thread while on the other thread, the while loop is running
            Console.WriteLine("press a key");
            Console.ReadKey();

            //the moment a key is pressed, the main thread will resumed
            //set the following shared variable to true
            shoud_i_stop = true;

            //the thread resumes
            //wait for the main thread to finish and resume thread
            t2.Join();

            //lets prevent the console from closing
            Console.ReadLine();

        }
    }
}
